from django.conf.urls import url
from rango import views


#urlpatterns tuple contains a series o calls to django.conf.urls.url() function
urlpatterns=[
            url(r'^$',views.index,name='index'),
            url(r'^about/$',views.about_page,name='About_Page'),
            url(r'^add_category/$',views.add_category,name='add_category'),
            url(r'^category/(?P<category_name_url>\w+)/$', views.category,name='category'),
            url(r'^category/(?P<category_name_url>\w+)/add_page/$',views.add_page,name='add_page'),
            ]
